﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lesson.Models
{
    public class Lesson
    {
        public int Day;
        public string LessonTitle;
        public string lessonContent;
        public string assessmentTitle;
        public string assessmentContent;
    }
}