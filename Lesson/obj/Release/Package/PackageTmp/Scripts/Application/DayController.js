﻿angular.module('app')
    .controller('landingController', ['$rootScope', '$scope', '$location', '$stateParams', 'lessonService', function ($rootScope, $scope, $location, $stateParams, lessonService) {
        if ($stateParams.userId != null)
            $rootScope.userId = parseInt($stateParams.userId, 10);
        $location.path('/day/1');
        $rootScope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };
    }])
    .controller('dayController', ['$rootScope', '$scope', '$location', '$stateParams', 'lessonService', function ($rootScope, $scope, $location, $stateParams, lessonService) {
        lessonService.startLoader();

        lessonService.getLessonOfDay($stateParams.day).then(function (results) {
            if (results.data.length > 0) {
                $scope.lessonId = results.data[0]._id['$oid'];
                $scope.assessmentContent = results.data[0].assessmentContent;
                $scope.assessmentTitle = results.data[0].assessmentTitle;
                $scope.lessonContent = results.data[0].lessonContent;
                $scope.lessonTitle = results.data[0].lessonTitle;
            }

            lessonService.getNotesOfLesson($scope.lessonId, $rootScope.userId).then(function (results) {
                if (results.data.length > 0) {
                    $scope.lessonNoteId = results.data[0]._id['$oid'];
                    $scope.lessonNotes = results.data[0].notes;
                }
                lessonService.stopLoader();
            }, function (results) {
                console.log(results);
                lessonService.stopLoader();
            });
        }, function (results) {
            console.log(results);
            lessonService.stopLoader();
        });


        $scope.runInitialScripts = function () {
            $('.sidebar-tabs').tabCollapse({
                tabsClass: 'visible-tabs',
                accordionClass: 'hidden-tabs'
            });

            $('.content-tabs').tabCollapse();

            $('.nav-tabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            $('.panel-group').on('shown.bs.collapse', function () {
                var panel = $(this).find('.in');
                $('html, body').animate({
                    scrollTop: panel.offset().top + (-60)
                }, 500);
            });
        };

        $scope.updateNotes = function () {
            lessonService.startLoader();

            var lessonNotesData = JSON.stringify({
                lessonId: $scope.lessonId,
                userId: $rootScope.userId,
                notes: $scope.lessonNotes
            });

            if ($scope.lessonNoteId != null) {
                lessonService.updateLessonNotes(lessonNotesData, $scope.lessonNoteId).then(function (results) {
                    console.log(results);
                    lessonService.stopLoader();
                }, function (results) {
                    console.log(results);
                    lessonService.stopLoader();
                });
            }
            else {
                lessonService.insertLessonNotes(lessonNotesData).then(function (results) {
                    $scope.lessonNoteId = results.data._id['$oid'];
                    lessonService.stopLoader();
                }, function (results) {
                    console.log(results);
                    lessonService.stopLoader();
                });
            }
        };

        setTimeout(function () {
            $scope.runInitialScripts();
        }, 300);
    }])
