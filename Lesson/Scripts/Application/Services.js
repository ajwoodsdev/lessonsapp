﻿'use strict'

angular.module('app').service('lessonService', ['$http', function ($http) {

    /* GET Methods */
    this.getLessonOfDay = function (day) {
        return $http.get('https://api.mlab.com/api/1/databases/vks9009/collections/Lesson?q={"day":' + day + '}&apiKey=jHAqLBnyacHGup75No_DeJEJs10CpO42');
    };

    this.getNotesOfLesson = function (lessonId, userId) {
        return $http.get('https://api.mlab.com/api/1/databases/vks9009/collections/LessonNotes?q={"lessonId":"' + lessonId + '","userId":' + userId + '}&apiKey=jHAqLBnyacHGup75No_DeJEJs10CpO42');
    };

    /* POST Methods */
    this.insertLessonNotes = function (notes) {
        return $http.post('https://api.mlab.com/api/1/databases/vks9009/collections/LessonNotes?apiKey=jHAqLBnyacHGup75No_DeJEJs10CpO42', notes);
    };

    this.updateLessonNotes = function (notes, lessonNoteId) {
        return $http.put('https://api.mlab.com/api/1/databases/vks9009/collections/LessonNotes?q={"_id":{"$oid":"' + lessonNoteId + '"}}&apiKey=jHAqLBnyacHGup75No_DeJEJs10CpO42', notes);
    };

    /* Loader */
    this.startLoader = function () {
        $('loading').css('position', 'absolute');
        $('loading').height($('#ui-view').height());
        $('loading').width($('#ui-view').width());
        $('loading').css('left', '50%');
        $('loading').css('top', '50%');
        $('loading').html('<div style="z-index: 999;position: absolute;margin-top: 20%;margin-left: 50%;"><img src="/Content/images/ajax-loader.gif" />LOADING...</div>').show();
    };
    this.stopLoader = function () {
        $('loading').hide();
    };
}]);