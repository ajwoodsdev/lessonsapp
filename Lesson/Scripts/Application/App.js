﻿'use strict';

angular.module('app', ['ui.router'])
    .run(function ($rootScope) {
        $rootScope.userId = 2;
    })
    .config(['$stateProvider', '$locationProvider', function ($stateProvider, $locationProvider) {
        $stateProvider
            .state('Landing', {
                url: '/:userId',
                templateUrl: '/Lesson/Landing',
                controller: 'landingController'
            })
            .state('day', {
                url: '/day/:day',
                templateUrl: '/Lesson/day',
                controller: 'dayController'
            })
            .state('otherwise', {
                url: '*path',
                templateUrl: '/views/404',
                controller: 'Error404Ctrl'
            });

        $locationProvider.html5Mode(true);

    }])
