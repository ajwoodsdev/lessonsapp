app.Carousel = (function() {
	"use strict";

	var DEFAULTS = {};
	return {
		initialize: function() {
			if( $('.featured-container').length ) {
				var carousels = $('.featured-container');				
				$.each(carousels, function( index, value ) {
					var id = $(value).attr('id'),
						el = $('#'+id+' li');

					//defining li width before passing them into default object
					if( $(window).width() < 991 ) {
						$(el).each(function() {
							if( $(this).parents('.four-boxes').length ) {
								$(this).css({ 'width':  Math.floor($('#'+id).outerWidth() / 3) });
							} else {
								$(this).css({ 'width':  Math.floor($('#'+id).outerWidth() / 2) });
							}
						});
					}

					if( $(window).width() < 751 ) {
						$(el).each(function() {
							if( $(this).parents('.four-boxes').length ) {
								$(this).css({ 'width':  Math.floor($('#'+id).outerWidth() / 2) });
							}
						});
					}

					if( $(window).width() < 511 ) {
						$(el).each(function() {
							$(this).css({ 'width':  $('#'+id).outerWidth() });
						});
					}

					DEFAULTS[id] = {
						totalElements: el.length,
						elementWidth: el.outerWidth(),
						parentNode: el.parent(), //ul
						parentNodeWidth: el.length * el.outerWidth(), //need to define ul width on load
						containerWidth: $('#'+id).outerWidth(),
						count: 0
					};

					//ul width
					DEFAULTS[id]['parentNode'].css( { 'width': ( DEFAULTS[id]['parentNodeWidth'] ), 'left': 0 } );

					//showing and hiding next prev links
					$('#'+id+' .links').hide();
					if( DEFAULTS[id]['parentNodeWidth'] > DEFAULTS[id]['containerWidth'] ) $('#'+id+' .next').show(); 
				});

				//defining max height for the featured container
				this.defineHeight();
			}
			return this;
		},
		nextSlide: function( e ) {
			e.preventDefault();
			var id = $(e.target.parentNode).attr('id'),
				position = DEFAULTS[id]['parentNode'].css('left'),
				parentNodePosition = parseInt(position.split('px')[0]),
				totalCount = Math.ceil( DEFAULTS[id]['parentNodeWidth'] / DEFAULTS[id]['containerWidth'] ) - 1 ;

			if( DEFAULTS[id]['count'] < totalCount ) {
				$('#'+id+' ul').animate( { left: parentNodePosition - DEFAULTS[id]['containerWidth'] } );
				DEFAULTS[id]['count']++;
				$('#'+id+' .prev').show();
			}

			if( DEFAULTS[id]['count'] === totalCount ) {
				$('#'+id+' .next').hide();
			}
			return this;
		},
		prevSlide: function( e ) {
			e.preventDefault();
			var	id = $(e.target.parentNode).attr('id'),
				position = DEFAULTS[id]['parentNode'].css('left'),
				parentNodePosition = parseInt(position.split('px')[0]);

			if( DEFAULTS[id]['count'] > 0 ) {
				$('#'+id+' ul').animate( { left: parentNodePosition + DEFAULTS[id]['containerWidth'] } );
				DEFAULTS[id]['count']--;
				$('#'+id+' .next').show();
			}

			if( DEFAULTS[id]['count'] === 0 ) {
				$('#'+id+' .prev').hide();
			}
			return this;
		},
		defineHeight: function() {
			var featuredContainer = $(".featured-container"),
				container = featuredContainer.find('li > div'),
				mediaBody = featuredContainer.find('li .media'),
				listing = (mediaBody.length) ? mediaBody : container;
			
			listing.css({ 'min-height': 0 });
	    	var max = -1;
	    	listing.each(function() {
			    var h = $(this).height(); 
			    max = h > max ? h : max;
			});

			if( mediaBody.length ) listing.css({ 'min-height': max });
			else listing.css({ 'min-height': max+35 });
		},
		swipeBoxes: function() {
			var self = this;
			$(".featured-carousel").swiperight(function() {
				$(this).parent().find('.prev').click();
			});

			$(".featured-carousel").swipeleft(function() {
				$(this).parent().find('.next').click();
			});
		}
	};
})();
